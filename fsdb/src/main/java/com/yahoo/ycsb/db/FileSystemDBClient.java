/**
 * Copyright (c) 2013 - 2016 YCSB contributors. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you
 * may not use this file except in compliance with the License. You
 * may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * permissions and limitations under the License. See accompanying
 * LICENSE file.
 */

package com.yahoo.ycsb.db;

import com.yahoo.ycsb.ByteIterator;
import com.yahoo.ycsb.DB;
import com.yahoo.ycsb.DBException;
import com.yahoo.ycsb.Status;
import com.yahoo.ycsb.db.workloads.FSCoreWorkload;

import java.util.*;
import java.io.*;

// zevin{
/**
 * A class that wraps the FileSystemDBClient to allow it to be interfaced with YCSB.
 * This class extends {@link DB} and implements the database interface used by YCSB client.
 */
public class FileSystemDBClient extends DB {
  public static final String CONFIG_FILE_DEFAULT = "/etc/ceph/ceph.conf";

  @Override
  public void init() throws DBException {
  }

  /**
   * Shutdown the client.
   */
  @Override
  public void cleanup() {

  }

  /**
   * Read the file.
   * @param dir
   * @param fileName
   * @param fields The list of fields to read, or null for all of them
   * @param result A HashMap of field/value pairs for the result
   * @return
   */
  @Override
  public Status read(final String dir, final String fileName, final Set<String> fields,
                     final Map<String, ByteIterator> result) {
    File f = new File(dir + '/' + fileName);
//    System.err.println("dir : " + dir + "\nfileName : " + fileName);
    long begin = System.nanoTime();
    try(InputStream out = new FileInputStream(f)) {
      if(FSCoreWorkload.openonly == false) {
        out.read();
      }
      long end = System.nanoTime();
      long nanoseconds = end - begin;
      FSCoreWorkload.stringBuffer.append("read-" + nanoseconds + "\n");
      if(FSCoreWorkload.stdOut == true){
        System.out.println("[READ] - " + dir + '/' + fileName + " " + nanoseconds + "ns");
      }
    }
    catch (IOException e) {
      e.printStackTrace();
      System.err.println("[READ FAILED] - " + dir + '/' + fileName);
      return Status.ERROR;
    }
    return Status.OK;
  }

  @Override
  public Status scan(final String dir, final String startkey, final int recordcount, final Set<String> fields,
                     final Vector<HashMap<String, ByteIterator>> result) {
    return Status.NOT_IMPLEMENTED;
  }

  /**
   * Write a file.
   * @param dir
   * @param fileName
   * @param values A HashMap of field/value pairs to update in the record
   * @return
   */
  @Override
  public Status update(final String dir, final String fileName, final Map<String, ByteIterator> values) {
    // append-only
    return appendFile(dir, fileName, 4*1024);
  }

  @Override
  public Status insert(final String dir, final String fileName, final Map<String, ByteIterator> values) {
    return createFile(dir, fileName);
  }

  @Override
  public Status delete(final String table, final String key) {
    return Status.NOT_IMPLEMENTED;
  }

  /**
   * Create a file.
   * @param dir
   * @param fileName
   * @return
   */
  public Status createFile(String dir, String fileName){
    File f = new File(dir + '/' + fileName);
    try{
      f.createNewFile();
      if(FSCoreWorkload.stdOut == true){
        System.out.println("[CREATE] - " + dir + '/' + fileName);
      }
    } catch(IOException e){
      e.printStackTrace();
      System.err.println(dir + '/' + fileName);
      return Status.ERROR;
    }
    return Status.OK;
  }

  /**
   * Append a string to the file.
   * @param dir
   * @param fileName
   * @param numBytes
   * @return
   */
  private Status appendFile(String dir, String fileName, int numBytes){
    long begin = System.nanoTime();
    try{
      BufferedWriter out = new BufferedWriter(new FileWriter(dir + '/' + fileName, true));
      if(FSCoreWorkload.openonly == false){
        String appendStr = String.join("", Collections.nCopies(numBytes, "a"));
        out.write(appendStr);
      }
      long end = System.nanoTime();
      long nanoseconds = end - begin;
      FSCoreWorkload.stringBuffer.append("write-" + nanoseconds + "\n");
      out.close();
      if(FSCoreWorkload.stdOut == true){
        System.out.println("[Append] - " + dir + '/' + fileName + " " + nanoseconds + "ns");
      }
//      System.err.println("[Append] - " + dir + '/' + fileName);
    } catch (IOException e){
      e.printStackTrace();
      System.err.println("[Append Failed] - " + dir + '/' + fileName);
      return Status.ERROR;
    }
    return Status.OK;
  }
}
// zevin}

package com.yahoo.ycsb.db.workloads;

import com.yahoo.ycsb.*;
import com.yahoo.ycsb.generator.*;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

/**
 * Something.
 */
public class FSCoreWorkload extends Workload {

  // if flush the output to std
  public static final String STDOUT_PROPERTY = "stdout";
  public static final String STDOUT_PROPERTY_DEFAULT = "true";
  public static boolean stdOut;

  // the number of the ceph-client
  public static final String NUM_MOUNT_PROPERTY = "numMount";
  public static final String NUM_MOUNT_PROPERTY_DEFAULT = "1";
  protected int numMount;

  // in mac:        /Users/king.zevin/usertable/op
  // in gpu nodes:  /mnt/ceph-client1/op

  // the path to which ceph-clients mount
  public static final String MOUNTDIR_PROPERTY = "mountDir";
  public static final String MOUNTDIR_PROPERTY_DEFAULT = "/mnt/ceph-client";
  protected String mountDir;
  protected String[] mountDirs;

  // the number of the opDirs
  public static final String NUM_OPDIR_PROPERTY = "numOpDir";
  public static final String NUM_OPDIR_PROPERTY_DEFAULT = "1";
  protected int numOpDir;

  // the path to which the the operations of the workload will be done
  public static final String OPDIR_PROPERTY = "opDir";
  public static final String OPDIR_PROPERTY_DEFAULT = "op";
  protected String opDir;
  protected String[] opDirs;

  // the path to dump the outputs such as latencies
  public static final String DUMPDIR_PROPERTY = "dumpDir";
  public static final String DUMPDIR_PROPERTY_DEFAULT = "dump";
  public static String dumpDir;

  public static StringBuffer stringBuffer;

  // the number of the prefixes of file names
  public static final String NUM_PREFIX_NAME_PROPERTY = "numPrefixName";
  public static final String NUM_PREFIX_NAME_PROPERTY_DEFAULT = "1";
  protected int numPrefixName;

  // the pre-prefix of file names -- one workload one pre-prefix name
  public static final String PREPREFIX_NAME_PROPERTY = "prePrefixName";
  public static final String PREPREFIX_NAME_PROPERTY_DEFAULT = "";
  protected String prePrefixName;

  public static final String APPEND_ONLY_PROPERTY = "appendonly";
  public static final String APPEND_ONLY_PROPERTY_DEFAULT = "true";
  public static boolean appendonly;

  public static final String OPEN_ONLY_PROPERTY = "openonly";
  public static final String OPEN_ONLY_PROPERTY_DEFAULT = "true";
  public static boolean openonly;

  public static final String INSERTION_RETRY_LIMIT = "insertion_retry_limit";
  public static final String INSERTION_RETRY_LIMIT_DEFAULT = "0";
  protected int insertionRetryLimit;

  public static final String INSERTION_RETRY_INTERVAL = "insertion_retry_interval";
  public static final String INSERTION_RETRY_INTERVAL_DEFAULT = "3";
  protected int insertionRetryInterval;

  public static final String READ_PROPORTION_PROPERTY = "readproportion";
  public static final String READ_PROPORTION_PROPERTY_DEFAULT = "0.9";

  public static final String UPDATE_PROPORTION_PROPERTY = "updateproportion";
  public static final String UPDATE_PROPORTION_PROPERTY_DEFAULT = "0.1";

  public static final String INSERT_PROPORTION_PROPERTY = "insertproportion";
  public static final String INSERT_PROPORTION_PROPERTY_DEFAULT = "0.0";

  /**
   * The name of the property for the the distribution of requests across the keyspace. Options are
   * "uniform", "zipfian" and "latest"
   */
  public static final String REQUEST_DISTRIBUTION_PROPERTY = "requestdistribution";
  /**
   * The default distribution of requests across the keyspace.
   */
  public static final String REQUEST_DISTRIBUTION_PROPERTY_DEFAULT = "uniform";

  /**
   * Percentage data items that constitute the hot set.
   */
  public static final String HOTSPOT_DATA_FRACTION = "hotspotdatafraction";

  /**
   * Default value of the size of the hot set.
   */
  public static final String HOTSPOT_DATA_FRACTION_DEFAULT = "0.2";

  /**
   * Percentage operations that access the hot set.
   */
  public static final String HOTSPOT_OPN_FRACTION = "hotspotopnfraction";

  /**
   * Default value of the percentage operations accessing the hot set.
   */
  public static final String HOTSPOT_OPN_FRACTION_DEFAULT = "0.8";


  protected NumberGenerator keysequence;
  protected DiscreteGenerator operationchooser;
  protected NumberGenerator keychooser;
  protected AcknowledgedCounterGenerator transactioninsertkeysequence;
  protected long recordcount;

  @Override
  public void init(Properties p) throws WorkloadException {

    stdOut = Boolean.parseBoolean(p.getProperty(STDOUT_PROPERTY, STDOUT_PROPERTY_DEFAULT));
    // number of ceph-clients
    numMount = Integer.parseInt(p.getProperty(NUM_MOUNT_PROPERTY, NUM_MOUNT_PROPERTY_DEFAULT));
    // numOpDir not yet used specifically
    numOpDir = Integer.parseInt(p.getProperty(NUM_OPDIR_PROPERTY, NUM_OPDIR_PROPERTY_DEFAULT));
    numPrefixName = Integer.parseInt(p.getProperty(NUM_PREFIX_NAME_PROPERTY, NUM_PREFIX_NAME_PROPERTY_DEFAULT));
    prePrefixName = p.getProperty(PREPREFIX_NAME_PROPERTY, PREPREFIX_NAME_PROPERTY_DEFAULT);
    opDir = p.getProperty(OPDIR_PROPERTY, OPDIR_PROPERTY_DEFAULT);

    String os = System.getProperty("os.name");
    System.out.println(os + "!");

    if(numMount == 1) {
      mountDirs = null;
      mountDir = p.getProperty(MOUNTDIR_PROPERTY, MOUNTDIR_PROPERTY_DEFAULT);
      if (os.compareTo("Mac OS X") == 0) {
        String[] s = mountDir.split("/");
        mountDir = "/Users/king.zevin/" + s[s.length - 1] + "-1";
      }
    } else if(numMount > 1){
      mountDir = null;
      mountDirs = new String[numMount];
      for(int i = 0; i < numMount; i++){
        mountDirs[i] = p.getProperty(MOUNTDIR_PROPERTY + (i+1), MOUNTDIR_PROPERTY_DEFAULT + "-" +(i + 1));
      }
      if (os.compareTo("Mac OS X") == 0){
        for(int i = 0; i < numMount; i++) {
          String[] s = mountDirs[i].split("/");
          mountDirs[i] = "/Users/king.zevin/" + s[s.length - 1];
        }
      }
    } else{
      System.err.println("numMount < 0!!!!");
      System.exit(-1);
    }

    dumpDir = p.getProperty(DUMPDIR_PROPERTY, DUMPDIR_PROPERTY_DEFAULT);
    if (os.compareTo("Mac OS X") == 0) {
      dumpDir = "/Users/king.zevin/" + dumpDir;
    } else{
      dumpDir = "/home/zevin/" + dumpDir;
    }
    stringBuffer = new StringBuffer();

      // append-only
    appendonly = Boolean.parseBoolean(p.getProperty(APPEND_ONLY_PROPERTY, APPEND_ONLY_PROPERTY_DEFAULT));

    // open-only
    openonly = Boolean.parseBoolean(p.getProperty(OPEN_ONLY_PROPERTY, OPEN_ONLY_PROPERTY));

    // recordcount
    recordcount =
        Long.parseLong(p.getProperty(Client.RECORD_COUNT_PROPERTY, "0"));
    if (recordcount == 0) {
      recordcount = 10000;
    }

    String requestdistrib =
        p.getProperty(REQUEST_DISTRIBUTION_PROPERTY, REQUEST_DISTRIBUTION_PROPERTY_DEFAULT);


    // for multi-threads???
    long insertstart =
        Long.parseLong(p.getProperty(INSERT_START_PROPERTY, INSERT_START_PROPERTY_DEFAULT));
    long insertcount=
        Integer.parseInt(p.getProperty(INSERT_COUNT_PROPERTY, String.valueOf(recordcount - insertstart)));
    // Confirm valid values for insertstart and insertcount in relation to recordcount
    if (recordcount < (insertstart + insertcount)) {
      System.err.println("Invalid combination of insertstart, insertcount and recordcount.");
      System.err.println("recordcount must be bigger than insertstart + insertcount.");
      System.exit(-1);
    }


    keysequence = new CounterGenerator(insertstart);
    operationchooser = createOperationGenerator(p);

    transactioninsertkeysequence = new AcknowledgedCounterGenerator(recordcount);
    if (requestdistrib.compareTo("uniform") == 0) {
      keychooser = new UniformLongGenerator(insertstart, insertstart + insertcount - 1);
    } else if (requestdistrib.compareTo("sequential") == 0) {
      keychooser = new SequentialGenerator(insertstart, insertstart + insertcount - 1);
    } else if (requestdistrib.compareTo("zipfian") == 0) {
      keychooser = new ZipfianGenerator(recordcount);
    } else if (requestdistrib.compareTo("latest") == 0) {
      keychooser = new SkewedLatestGenerator(transactioninsertkeysequence);
    } else if (requestdistrib.equals("hotspot")) {
      double hotsetfraction =
          Double.parseDouble(p.getProperty(HOTSPOT_DATA_FRACTION, HOTSPOT_DATA_FRACTION_DEFAULT));
      double hotopnfraction =
          Double.parseDouble(p.getProperty(HOTSPOT_OPN_FRACTION, HOTSPOT_OPN_FRACTION_DEFAULT));
      keychooser = new HotspotIntegerGenerator(insertstart, insertstart + insertcount - 1,
          hotsetfraction, hotopnfraction);
    } else {
      throw new WorkloadException("Unknown request distribution \"" + requestdistrib + "\"");
    }


    insertionRetryLimit = Integer.parseInt(p.getProperty(INSERTION_RETRY_LIMIT, INSERTION_RETRY_LIMIT_DEFAULT));
    insertionRetryInterval = Integer.parseInt(p.getProperty(
        INSERTION_RETRY_INTERVAL, INSERTION_RETRY_INTERVAL_DEFAULT));
  }

  @Override
  public Object initThread(Properties p, int mythreadid, int threadcount) throws WorkloadException {
    // path
    String path;
    String thisOpDir = opDir;
    if(numOpDir > 1){
      thisOpDir = opDir +"-" + ((mythreadid % numMount) + 1);
    }
    if(numMount > 1){
      int indexPath = mythreadid % numMount;
//      if(mythreadid >= threadcount / numMount * numMount){
//        return null;
//      } else{
      path = mountDirs[indexPath] + "/" + thisOpDir;
//      }
    } else{
      path = mountDir + "/" + thisOpDir;
    }


    // prefix
    String prefix = "";
    if(numPrefixName > 1){
      int indexPrefix = mythreadid % numPrefixName;
//      if(mythreadid >= threadcount / numPrefixName * numPrefixName){
//        return null;
//      } else{
      prefix = Character.toString((char)('a' + indexPrefix));
//      }
    }
    return new ThreadState(path, prefix);
  }

  @Override
  public void cleanup() throws WorkloadException {
    try {
      BufferedWriter out = new BufferedWriter(new FileWriter(dumpDir, true));
      out.write(stringBuffer.toString());
      out.close();
//      System.out.println(stringBuffer.toString());
    } catch (IOException e){
      e.printStackTrace();
      System.err.println("[DUMP Failed] - " + dumpDir);
    }
  }

  /**
   * Do one insert operation. Because it will be called concurrently from multiple client threads,
   * this function must be thread safe. However, avoid synchronized, or the threads will block waiting
   * for each other, and it will be difficult to reach the target throughput. Ideally, this function would
   * have no side effects other than DB operations.
   */
  @Override
  public boolean doInsert(DB db, Object threadstate){
    String path = ((ThreadState)threadstate).path;
    String prefix = ((ThreadState)threadstate).prefix;

    int keynum = keysequence.nextValue().intValue();
    String fileName = buildFileName(prefix, keynum);

    Status status;
    int numOfRetries = 0;
    while(true){
      status = db.insert(path, fileName, null);
      if (null != status && status.isOk()){
        break;
      }
      if(++numOfRetries <= insertionRetryLimit){
        System.err.println("Retrying insertion, retry count: " + numOfRetries);
        try {
          // Sleep for a random number between [0.8, 1.2)*insertionRetryInterval.
          int sleepTime = (int) (1000 * insertionRetryInterval * (0.8 + 0.4 * Math.random()));
          Thread.sleep(sleepTime);
        } catch (InterruptedException e) {
          break;
        }
      } else {
        System.err.println("Error inserting, not retrying any more. number of attempts: " + numOfRetries +
            "Insertion Retry Limit: " + insertionRetryLimit);
        break;
      }
    }
    return null != status && status.isOk();
  }

  @Override
  public boolean doTransaction(DB db, Object threadstate){
    String operation = operationchooser.nextString();
    if(operation == null){
      return false;
    }

    switch (operation) {
    case "UPDATE":
      doTransactionUpdate(db, threadstate);
      break;
    case "INSERT":
      doTransactionInsert(db, threadstate);
      break;
    case "CREATEFILE":
      doTransactionInsert(db, threadstate);
      break;
    case "READ":
    default:
      doTransactionRead(db, threadstate);
    }

    return true;
  }

  public void doTransactionRead(DB db, Object threadstate) {
    String path = ((ThreadState)threadstate).path;
    String prefix = ((ThreadState)threadstate).prefix;

    // choose a random key
    long keynum = nextKeynum();

    String fileName = buildFileName(prefix, keynum);

    db.read(path, fileName, null, null);
  }

  public void doTransactionUpdate(DB db, Object threadstate) {
    String path = ((ThreadState)threadstate).path;
    String prefix = ((ThreadState)threadstate).prefix;

    // choose a random key
    long keynum = nextKeynum();

    String fileName = buildFileName(prefix, keynum);

    db.update(path, fileName, null);
  }

  public void doTransactionInsert(DB db, Object threadstate) {
    String path = ((ThreadState)threadstate).path;
    String prefix = ((ThreadState)threadstate).prefix;

    // choose the next key
    long keynum = transactioninsertkeysequence.nextValue();

    try {
      String fileName = buildFileName(prefix, keynum);
      db.insert(path, fileName, null);
    } finally {
      transactioninsertkeysequence.acknowledge(keynum);
    }
  }

  /**
   * Creates a weighted discrete values with database operations for a workload to perform.
   * Weights/proportions are read from the properties list and defaults are used
   * when values are not configured.
   * Current operations are "READ", "UPDATE", "INSERT", "SCAN" and "READMODIFYWRITE".
   *
   * @param p The properties list to pull weights from.
   * @return A generator that can be used to determine the next operation to perform.
   * @throws IllegalArgumentException if the properties object was null.
   */
  protected static DiscreteGenerator createOperationGenerator(final Properties p) {
    if (p == null) {
      throw new IllegalArgumentException("Properties object cannot be null");
    }
    final double readproportion = Double.parseDouble(
        p.getProperty(READ_PROPORTION_PROPERTY, READ_PROPORTION_PROPERTY_DEFAULT));
    final double updateproportion = Double.parseDouble(
        p.getProperty(UPDATE_PROPORTION_PROPERTY, UPDATE_PROPORTION_PROPERTY_DEFAULT));
    final double insertproportion = Double.parseDouble(
        p.getProperty(INSERT_PROPORTION_PROPERTY, INSERT_PROPORTION_PROPERTY_DEFAULT));

    final DiscreteGenerator operationchooser = new DiscreteGenerator();
    if (readproportion > 0) {
      operationchooser.addValue(readproportion, "READ");
    }

    if (updateproportion > 0) {
      operationchooser.addValue(updateproportion, "UPDATE");
    }

    if (insertproportion > 0) {
      operationchooser.addValue(insertproportion, "INSERT");
    }

    return operationchooser;
  }


  /**
   * Builds a file name.
   * @param keynum
   * @return
   */
  protected String buildFileName(String prefix, long keynum) {
    String value = Long.toString(keynum  / numPrefixName);
    return prePrefixName + prefix + "F" + value;
  }

  long nextKeynum(){
    long keynum;
    if (keychooser instanceof ExponentialGenerator) {
      do {
        keynum = transactioninsertkeysequence.lastValue() - keychooser.nextValue().intValue();
      } while (keynum < 0);
    } else {
      do {
        keynum = keychooser.nextValue().intValue();
      } while (keynum > transactioninsertkeysequence.lastValue());
    }
    return keynum;
  }




  protected class ThreadState{
    protected String prefix = "";
    protected String path = "";
    protected ThreadState(final String path, final String prefix){
      this.path = path;
      this.prefix = prefix;
    }
  }

}


/*
  todo: 1. mountDir+opDir;
  todo: 2. new class ThreadState;
*/

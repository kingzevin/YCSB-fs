package com.yahoo.ycsb.db;

import com.yahoo.ycsb.db.workloads.OverleafCoreWorkload;
//import org.apache.http.Header;
import org.apache.http.client.CookieStore;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
//import org.apache.http.cookie.Cookie;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
//import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
//import org.apache.http.client.methods.HttpPost;
import org.apache.commons.codec.Charsets;
import org.codehaus.jackson.map.ObjectMapper;
//import org.java_websocket.WebSocket;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import org.json.simple.JSONArray;
//import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
//import java.util.ArrayList;
import java.util.HashMap;
//import java.util.List;
import java.util.Map;
//import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Something.
 */
public class OverleafClient {
  public String userID;
  public String userEmail;
  public String projectID;
  public String docID;
  public String rootDocID;
  public String rootFolderID;
  public String docVersion;
  public String docText;
  public String pendingText;
  public int threadID;
  public int counter = 0;
  public String singleTask;

  public Map<String, Object> waitingList = new HashMap<>();

  public String targetAddress = "localhost";
  private final String whisk19 = "192.168.122.149";
  private final String whisk18 = "192.168.122.148";
  public WebSocketClient wsClient;
  public CloseableHttpClient httpClient;
  public CookieStore cookieStore;
  public HttpContext httpContext;
  public String csrfToken;
  private String socketToken;
  private long seqCount = 1;

  public long timeStampTC1;
  public long timeStampTC2;
  public long nanoseconds;
  private OverleafCoreWorkload.Logger logger;

  public OverleafClient(String port,
                        String userID,
                        String userEmail,
                        String projectID,
                        String docID,
                        int threadID,
                        String singleTask){
    this.userID = userID;
    this.userEmail = userEmail;
    this.projectID = projectID;
    this.docID = docID;
    this.threadID = threadID;
    this.singleTask = singleTask;

    String os = System.getProperty("os.name");
    if (os.compareTo("Mac OS X") != 0) {
      // whisk
      if(port.compareTo("9443") == 0){
        targetAddress = whisk19;
      } else {
        targetAddress = whisk18;
      }
    }
    targetAddress += ":" + port;
    RequestConfig requestConfig = RequestConfig.custom()
        .setExpectContinueEnabled(true)
        .setConnectTimeout(15000)
        .setSocketTimeout(15000)
        .setConnectionRequestTimeout(15000)
        .setCookieSpec(CookieSpecs.STANDARD).build(); // to parse the expire attribute correctly
    httpClient = HttpClients.custom().setDefaultRequestConfig(requestConfig).setMaxConnPerRoute(100000).build();
    cookieStore = new BasicCookieStore();
    httpContext = new BasicHttpContext();
    httpContext.setAttribute(HttpClientContext.COOKIE_STORE, cookieStore);
  }

  // Task Set - Begin
  // Task: login_get
  public void loginGetTask(){
    nanoseconds = 0;
    String path = "/login";
    CloseableHttpResponse response = httpGET(path);
    try{
      // csrf
      String res = EntityUtils.toString(response.getEntity(), Charsets.UTF_8);
      logger.printLog("L3 GET RESPONSE: ", res);
      csrfToken = findCsrfToken(res);
      logger.printLog("L3 CSRF", "csrfToken = " + csrfToken);
      if(csrfToken == null){
        System.exit(-1);
      }
      response.close();
    } catch(IOException e){
      System.err.println("IOException for loginGetTask");
      e.printStackTrace(System.err);
      System.exit(-1);
    }
  }

  // Task: login_post
  public void loginPostTask(){
    nanoseconds = 0;
    String path = "/login";

    try{
      HashMap values = new HashMap<String, String>() {{
          put("_csrf", csrfToken);
          put("email", userEmail);
          put("password", "123456");
        }};
      ObjectMapper objectMapper = new ObjectMapper();
      String body = objectMapper.writeValueAsString(values);
      logger.printLog("L3 BODY", body);
      CloseableHttpResponse response = httpPOST(path, body);

      String res = EntityUtils.toString(response.getEntity(), Charsets.UTF_8);
      logger.printLog("L3 POST RESPONSE: ", res);
      String token = findCsrfToken(res);
      csrfToken = token == null ? csrfToken : token;
      logger.printLog("L3 CSRF", "csrfToken = " + csrfToken);
      response.close();
    } catch(IOException e){
      System.err.println("IOException for loginPostTask");
      e.printStackTrace(System.err);
      System.exit(-1);
    }
  }

  // Task: socket get
  public void socketGetTask(){
    nanoseconds = 0;
    String socketPath = "/socket.io/1/?t=" + System.currentTimeMillis();
    try{
      CloseableHttpResponse response = httpGET(socketPath);
      // csrf
      String res = EntityUtils.toString(response.getEntity(), Charsets.UTF_8);
      logger.printLog("L3 GET RESPONSE: ", res);
      socketToken = res.split(":")[0];
      logger.printLog("L3 0 GET SOCKET: ", socketToken);
      String token = findCsrfToken(res);
      csrfToken = token == null ? csrfToken : token;
      logger.printLog("L3 CSRF", "csrfToken = " + csrfToken);
      response.close();
    } catch(Exception e){
      System.err.println("Exception for response.getEntity: ");
      e.printStackTrace(System.err);
      System.exit(-1);
      throw new FailError();
    }
  }

  // Task: socket_connect
  public void socketConnectTask() {
    nanoseconds = 0;
    final boolean[] failFlag = {false};
    long begin = 0, end;

    URI socketURI = null;
    try{
      socketURI = new URI("ws://"+ targetAddress + "/socket.io/1/websocket/" + socketToken);
    } catch (URISyntaxException e){
      System.err.println("URISyntaxException: ");
      e.printStackTrace(System.err);
      failFlag[0] = true;
      System.exit(-1);
    }
    wsClient = new WebSocketClient(socketURI) {
      @Override
      public void onOpen(ServerHandshake serverHandshake) {
        logger.printLog("L0 WS", "Handshake");
      }

      @Override
      public void onMessage(String s) {
        logger.printLog("L1 WS", "Message: " + s);
        if(s.contains("otUpdateApplied")){
          logger.printLog("L1 otUpdateApplied", "Message: " + s);
          updateVersion(s);
          if(s.contains("user_id")){
            // not my edit
            return;
          }
          Object otUpdateAppliedFlag = waitingList.get("otUpdateAppliedFlag");
          if(otUpdateAppliedFlag == null){
            return;
          }
          synchronized (otUpdateAppliedFlag){
            otUpdateAppliedFlag.notify();
          }
        } else if(s.contains("2::")){
          // heartbeat
          wsClient.send("2:::");
        } else if(s.contains("connectionAccepted")) {
          wsClient.send("5:" + seqCount++
              + "+::{\"name\":\"joinProject\",\"args\":[{\"project_id\":\"" + projectID + "\"}]}");
        } else if(s.contains("6:::1+")){
          // ack: joinProject
          setStates1(s);
          wsClient.send("5:" + seqCount++
              + "+::{\"name\":\"joinDoc\",\"args\":[\"" + docID + "\",{\"encodeRanges\":true}]}");
        } else if(s.contains("6:::2+")){
          // ack: joinDoc
          setStates2(s);
          wsClient.send("5:" + seqCount++
              + "+::{\"name\":\"clientTracking.getConnectedUsers\"}");
        } else if(s.contains("6:::3+")){
          // ack: getConnectedUsers
          Object socketConnectedFlag = waitingList.get("socketConnectedFlag");
          synchronized (socketConnectedFlag){
            socketConnectedFlag.notify();
          }
        }
      }

      @Override
      public void onClose(int i, String s, boolean b) {
        logger.printLog("L9 WS", "Closed");
        failFlag[0] = true;
        if(singleTask.compareTo("edit_doc") == 0){
          System.exit(-1);
        }
      }

      @Override
      public void onError(Exception e) {
        logger.printLog("L9 WS", "Error:" + e);
        failFlag[0] = true;
        System.exit(-1);
      }
    };

    // wait for the connection to be established
    try{
      Object socketConnectedFlag = new Object();
      waitingList.put("socketConnectedFlag", socketConnectedFlag);
      begin = System.nanoTime(); // connection init start
      timeStampTC2 = System.currentTimeMillis();
      wsClient.connect();
      synchronized (socketConnectedFlag){
        socketConnectedFlag.wait();
      }
      waitingList.remove(socketConnectedFlag);
    } catch (InterruptedException e){
      failFlag[0] = true;
      System.err.println("InterruptedException: ");
      e.printStackTrace(System.err);
    }
    end = System.nanoTime(); // connection init end
    timeStampTC2 = System.currentTimeMillis();
    nanoseconds = end - begin;
    if(failFlag[0]){
      throw new FailError();
    }
  }

  // Task: edit_doc
  public void editDocTask(){
    nanoseconds = 0;
    boolean failFlag = false;
    long begin = 0, end;
//    String wordToChange = docText.substring(77, 78);
    String wordToChange = "Z";
    int a = ThreadLocalRandom.current().nextInt(0, 10);
    String newWord = "Z" + a;
    String updateMessage = "5:" + seqCount++ + "+::{\"name\":\"applyOtUpdate\",\"args\":[\""+ docID
        + "\",{\"doc\":\"" + docID + "\",\"op\":["
//        + "{\"p\":77,\"d\":\"" + wordToChange + "\"},"
        + "{\"p\":77,\"i\":\"" + "0" + "\"}" + "],\"v\":"
//        + "{\"p\":77,\"i\":\"" + newWord + "\"}" + "],\"v\":"
        + docVersion + ",\"lastV\":" + (Integer.parseInt(docVersion) - 1) + "}]}";

    pendingText = docText.replace(wordToChange, newWord);
    logger.printLog("L3 updateMessage", updateMessage);
    try{
      Object otUpdateAppliedFlag = new Object();
      waitingList.put("otUpdateAppliedFlag", otUpdateAppliedFlag);
      begin = System.nanoTime();
      timeStampTC1 = System.currentTimeMillis();
      wsClient.send(updateMessage);
      synchronized (otUpdateAppliedFlag){
        otUpdateAppliedFlag.wait();
      }
      waitingList.remove(otUpdateAppliedFlag);
    } catch (InterruptedException e){
      failFlag = true;
      System.err.println("InterruptedException: ");
      e.printStackTrace(System.err);
    }
    end = System.nanoTime();
    timeStampTC2 = System.currentTimeMillis();
    nanoseconds = end - begin;
    if(failFlag){
      throw new FailError();
    }
  }

  // Task: spell_check
  public void spellCheckTask(){
    nanoseconds = 0;
    String path = "/spelling/check";
    boolean failFlag = false;
    String wordGen = "misc";
    for (int i = 0; i < 10; i++) {
      wordGen = wordGen + (char)('a' + ThreadLocalRandom.current().nextInt(0, 26));
    }
    final String word = wordGen;
    try{
      HashMap values = new HashMap<String, String>() {{
          put("language", "en");
          put("token", userID);
          put("words", "[\"" + word +"\"]");
          put("_csrf", csrfToken);
        }};
      ObjectMapper objectMapper = new ObjectMapper();
      String body = objectMapper.writeValueAsString(values);
      logger.printLog("L3 BODY", body);
      CloseableHttpResponse response = httpPOST(path, body);
      if(response.getStatusLine().getStatusCode() < 200 || response.getStatusLine().getStatusCode() >= 300){
        logger.printLog("L9 spellCheckTask", "RES: " + response.getStatusLine().getStatusCode());
        failFlag = true;
      }
      response.close();
    } catch(IOException e){
      failFlag = true;
      System.err.println("IOException for spellCheckTask");
      e.printStackTrace(System.err);
    }
    if(failFlag){
      System.exit(-1);
      throw new FailError();
    }
  }

  // Task: chat_get
  public void chatGetTask(){
    nanoseconds = 0;
    String path = "/project/" + projectID + "/messages?limit=50";
    boolean failFlag = false;
    try{
      CloseableHttpResponse response = httpGET(path);
      if(response.getStatusLine().getStatusCode() < 200 || response.getStatusLine().getStatusCode() >= 300){
        logger.printLog("L9 chatGetTask", "RES: " + response.getStatusLine().getStatusCode());
        failFlag = true;
      }
      response.close();
    } catch (IOException e){
      failFlag = true;
      System.err.println("IOException for chatGetTask");
      e.printStackTrace(System.err);
    }

    if(failFlag){
      throw new FailError();
    }
  }

  // Task: chat_post
  public void chatPostTask(){
    nanoseconds = 0;
    String path = "/project/" + projectID + "/messages";
    boolean failFlag = false;
    try{
      HashMap values = new HashMap<String, String>() {{
          put("content", "Hello Marrio");
          put("_csrf", csrfToken);
        }};
      ObjectMapper objectMapper = new ObjectMapper();
      String body = objectMapper.writeValueAsString(values);
      CloseableHttpResponse response = httpPOST(path, body);
      if(response.getStatusLine().getStatusCode() < 200 || response.getStatusLine().getStatusCode() >= 300){
        logger.printLog("L9 chatPostTask", "RES: " + response.getStatusLine().getStatusCode());
        failFlag = true;
      }
      response.close();
    } catch (IOException e){
      failFlag = true;
      System.err.println("IOException for chatPostTask");
      e.printStackTrace(System.err);
    }

    if(failFlag){
      throw new FailError();
    }
  }

  // Task: email_get
  public void emailGetTask(){
    nanoseconds = 0;
    String path = "/user/emails";
    boolean failFlag = false;
    try{
      CloseableHttpResponse response = httpGET(path);
      if(response.getStatusLine().getStatusCode() < 200 || response.getStatusLine().getStatusCode() >= 300){
        logger.printLog("L9 emailGetTask", "RES: " + response.getStatusLine().getStatusCode());
        failFlag = true;
      }
      response.close();
    } catch (IOException e){
      failFlag = true;
      System.err.println("IOException for emailGetTask");
      e.printStackTrace(System.err);
    }

    if(failFlag){
      throw new FailError();
    }
  }

  // Task: historyUpdatesGetTask
  public void historyUpdatesGetTask(){
    nanoseconds = 0;
    String path = "/project/" + projectID + "/updates?min_count=10";
    boolean failFlag = false;
    try{
      CloseableHttpResponse response = httpGET(path);
      if(response.getStatusLine().getStatusCode() < 200 || response.getStatusLine().getStatusCode() >= 300){
        logger.printLog("L9 historyUpdatesGetTask", "RES: " + response.getStatusLine().getStatusCode());
        failFlag = true;
      }
      response.close();
    } catch (IOException e){
      failFlag = true;
      System.err.println("IOException for historyUpdatesGetTask");
      e.printStackTrace(System.err);
    }

    if(failFlag){
      throw new FailError();
    }
  }

  // Task: historyDiffGetTask
  public void historyDiffGetTask(){
    nanoseconds = 0;
    String path = "/project/" + projectID + "/doc/" + docID + "/diff?from=1&to=2";
    boolean failFlag = false;
    try{
      CloseableHttpResponse response = httpGET(path);
      if(response.getStatusLine().getStatusCode() < 200 || response.getStatusLine().getStatusCode() >= 300){
        logger.printLog("L9 historyDiffGetTask", "RES: " + response.getStatusLine().getStatusCode());
        failFlag = true;
      }
      response.close();
    } catch (IOException e){
      failFlag = true;
      System.err.println("IOException for historyDiffGetTask");
      e.printStackTrace(System.err);
    }

    if(failFlag){
      System.exit(-1);
      throw new FailError();
    }
  }

  // Task: registerGetTask
  public void registerGetTask(){
    nanoseconds = 0;
    String path = "/register";
    boolean failFlag = false;
    try{
      CloseableHttpResponse response = httpGET(path);
      if(response.getStatusLine().getStatusCode() < 200 || response.getStatusLine().getStatusCode() >= 300){
        logger.printLog("L9 registerGetTask", "RES: " + response.getStatusLine().getStatusCode());
        failFlag = true;
      }
      response.close();
    } catch (IOException e){
      failFlag = true;
      System.err.println("IOException for registerGetTask");
      e.printStackTrace(System.err);
    }

    if(failFlag){
      throw new FailError();
    }
  }

  // Task: settingsGetTask
  public void settingsGetTask(){
    nanoseconds = 0;
    String path = "/user/settings";
    boolean failFlag = false;
    try{
      CloseableHttpResponse response = httpGET(path);
      if(response.getStatusLine().getStatusCode() < 200 || response.getStatusLine().getStatusCode() >= 300){
        logger.printLog("L9 settingsGetTask", "RES: " + response.getStatusLine().getStatusCode());
        failFlag = true;
      }
      response.close();
    } catch (IOException e){
      failFlag = true;
      System.err.println("IOException for settingsGetTask");
      e.printStackTrace(System.err);
    }

    if(failFlag){
      throw new FailError();
    }
  }

  // Task: contactsGetTask
  public void contactsGetTask(){
    nanoseconds = 0;
    String path = "/user/contacts";
    boolean failFlag = false;
    try{
      CloseableHttpResponse response = httpGET(path);
      if(response.getStatusLine().getStatusCode() < 200 || response.getStatusLine().getStatusCode() >= 300){
        logger.printLog("L9 contactsGetTask", "RES: " + response.getStatusLine().getStatusCode());
        failFlag = true;
      }
      response.close();
    } catch (IOException e){
      failFlag = true;
      System.err.println("IOException for contactsGetTask");
      e.printStackTrace(System.err);
    }

    if(failFlag){
      throw new FailError();
    }
  }

  // Task: invitesGetTask
  public void invitesGetTask(){
    nanoseconds = 0;
    String path = "/project/" + projectID + "/invites";
    boolean failFlag = false;
    try{
      CloseableHttpResponse response = httpGET(path);
      if(response.getStatusLine().getStatusCode() < 200 || response.getStatusLine().getStatusCode() >= 300){
        logger.printLog("L9 invitesGetTask", "RES: " + response.getStatusLine().getStatusCode());
        failFlag = true;
      }
      response.close();
    } catch (IOException e){
      failFlag = true;
      System.err.println("IOException for invitesGetTask");
      e.printStackTrace(System.err);
    }

    if(failFlag){
      throw new FailError();
    }
  }

  // Task: project_get
  public void projectIDGetTask(){
    nanoseconds = 0;
    String path = "/project/" + projectID;
    boolean failFlag = false;
    try{
      CloseableHttpResponse response = httpGET(path);
      if(response.getStatusLine().getStatusCode() < 200 || response.getStatusLine().getStatusCode() >= 300){
        logger.printLog("L9 projectIDGetTask", "RES: " + response.getStatusLine().getStatusCode());
        failFlag = true;
      }
      response.close();
    } catch (IOException e){
      failFlag = true;
      System.err.println("IOException for projectIDGetTask");
      e.printStackTrace(System.err);
    }

    if(failFlag){
      throw new FailError();
    }
  }

  // Task: project_get
  public void projectGetTask(){
    nanoseconds = 0;
    String path = "/project";
    boolean failFlag = false;
    try{
      CloseableHttpResponse response = httpGET(path);
      if(response.getStatusLine().getStatusCode() < 200 || response.getStatusLine().getStatusCode() >= 300){
        logger.printLog("L9 projectGetTask", "RES: " + response.getStatusLine().getStatusCode());
        failFlag = true;
      }
      response.close();
    } catch (IOException e){
      failFlag = true;
      System.err.println("IOException for projectGetTask");
      e.printStackTrace(System.err);
    }

    if(failFlag){
      throw new FailError();
    }
  }

  // Task: compileTask
  public void compilePostTask() {
    nanoseconds = 0;
    String path = "/project/" + projectID + "/compile";
    boolean failFlag = false;
    try {
      HashMap values = new HashMap<String, String>() {{
          put("check", "silent");
          put("draft", "false");
          put("incrementalCompilesEnabled", "true");
          put("rootDoc_id", rootDocID);
          put("_csrf", csrfToken);
        }};
      ObjectMapper objectMapper = new ObjectMapper();
      String body = objectMapper.writeValueAsString(values);
      CloseableHttpResponse response = httpPOST(path, body);
      if (response.getStatusLine().getStatusCode() < 200 || response.getStatusLine().getStatusCode() >= 300) {
        logger.printLog("L9 compilePostTask", "RES: " + response.getStatusLine().getStatusCode());
        failFlag = true;
      }
      response.close();
    } catch (IOException e) {
      failFlag = true;
      System.err.println("IOException for compilePostTask");
      e.printStackTrace(System.err);
    }

    if (failFlag) {
      throw new FailError();
    }
  }

  // Task: compileTask
  public void invitePostTask(){
    nanoseconds = 0;
    String path = "/project/" + projectID + "/invite";
    boolean failFlag = false;
    try{
      HashMap values;
      values = new HashMap<String, String>() {{
          put("email", "zevin@qq.com"); // this email should only act as a invitation receiver.
          put("privileges", "readAndWrite");
          put("_csrf", csrfToken);
        }};
      ObjectMapper objectMapper = new ObjectMapper();
      String body = objectMapper.writeValueAsString(values);
      CloseableHttpResponse response = httpPOST(path, body);
      if(response.getStatusLine().getStatusCode() < 200 || response.getStatusLine().getStatusCode() >= 300){
        logger.printLog("L9 invitePostTask", "RES: " + response.getStatusLine().getStatusCode());
        failFlag = true;
      }
      response.close();
    } catch (IOException e){
      failFlag = true;
      System.err.println("IOException for invitePostTask");
      e.printStackTrace(System.err);
    }

    if(failFlag){
      throw new FailError();
    }
  }

  // Task: createPostTask
  public String createPostTask(){
    nanoseconds = 0;
    String path = "/project/new";
    boolean failFlag = false;
    String newProject = null;
    try{
      HashMap values = new HashMap<String, String>() {{
          put("projectName", "create_x" + counter++);
          put("template", "none");
          put("_csrf", csrfToken);
        }};
      ObjectMapper objectMapper = new ObjectMapper();
      String body = objectMapper.writeValueAsString(values);
      CloseableHttpResponse response = httpPOST(path, body);
      if(response.getStatusLine().getStatusCode() < 200 || response.getStatusLine().getStatusCode() >= 300){
        logger.printLog("L9 createPostTask", "RES: " + response.getStatusLine().getStatusCode());
        failFlag = true;
      }
      String resString = EntityUtils.toString(response.getEntity(), Charsets.UTF_8);
      if (resString.contains("project_id")){
        newProject = findProjectID(resString);
      }
      logger.printLog("L3 createPostTask", "RES: " + resString);
      response.close();
    } catch (IOException e){
      failFlag = true;
      System.err.println("IOException for createPostTask");
      e.printStackTrace(System.err);
    }

    if(failFlag){
      throw new FailError();
    }
    return newProject;
  }

  // Task: trashPostTask
  public void trashPostTask(String newProject){
    nanoseconds = 0;
    String path = "/project/" + newProject + "/trash";
    boolean failFlag = false;
    try{
      HashMap values = new HashMap<String, String>() {{
          put("_csrf", csrfToken);
        }};
      ObjectMapper objectMapper = new ObjectMapper();
      String body = objectMapper.writeValueAsString(values);
      CloseableHttpResponse response = httpPOST(path, body);
      if(response.getStatusLine().getStatusCode() < 200 || response.getStatusLine().getStatusCode() >= 300){
        logger.printLog("L9 trashPostTask", "RES: " + response.getStatusLine().getStatusCode());
        failFlag = true;
      }
      response.close();
    } catch (IOException e){
      failFlag = true;
      System.err.println("IOException for trashPostTask");
      e.printStackTrace(System.err);
    }

    if(failFlag){
      throw new FailError();
    }
  }

  // Task Set - End

  public CloseableHttpResponse httpGET(String path){
    boolean failFlag = false;
    long begin = 0, end = 0;
    String url = "http://" + targetAddress + path;
    HttpGet request = new HttpGet(url);
    CloseableHttpResponse response = null;
    logger.printLog("L3 OVCLIENT", "http get, path=" + url);
    try {
      request.addHeader("Accept", "*/*");
      request.addHeader("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) "
          + "AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36");
      request.addHeader("Referer", "http://localhost:8002/project/5fa8e2184c975f0093d351a5");
      begin = System.nanoTime();
      timeStampTC1 = System.currentTimeMillis();
      response = httpClient.execute(request, httpContext);
      end = System.nanoTime();
      timeStampTC2 = System.currentTimeMillis();
    } catch (IOException e){
      failFlag = true;
      System.err.println("IOException for httpGET");
      e.printStackTrace(System.err);
      System.exit(-1);
    }
    nanoseconds = end - begin;
    if(failFlag){
      throw new FailError();
    }
    return response;
  }

  public CloseableHttpResponse httpPOST(String path, String body){
    long begin = 0, end = 0;
    String url = "http://" + targetAddress + path;
    HttpPost request = new HttpPost(url);
    CloseableHttpResponse response = null;
    logger.printLog("L1 HTTP-POST", "path=" + url + ";body=" + body);

    try {
      request.setEntity(new StringEntity(body));
      request.addHeader("Accept", "*/*");
      request.addHeader("Content-type", "application/json");
      request.addHeader("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) "
          + "AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36");
      request.addHeader("Referer", "http://localhost:8002/project/5fa8e2184c975f0093d351a5");
      begin = System.nanoTime();
      timeStampTC1 = System.currentTimeMillis();
      response = httpClient.execute(request, httpContext);
      timeStampTC2 = System.currentTimeMillis();
      end = System.nanoTime();
    } catch (Exception e){
      System.err.println("IOException for httpPOST");
      e.printStackTrace(System.err);
      System.exit(-1);
    }
    nanoseconds = end - begin;
    return response;
  }
  public CloseableHttpResponse httpDELETE(String path){
    long begin = 0, end = 0;
    String url = "http://" + targetAddress + path + "?forever=true";
    HttpDelete request = new HttpDelete(url);
    CloseableHttpResponse response = null;
    logger.printLog("L1 HTTP-DELETE", "path=" + url);

    try {
      request.addHeader("Accept", "*/*");
      request.addHeader("Content-type", "application/json");
      request.addHeader("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_6) "
          + "AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.193 Safari/537.36");
      request.addHeader("Referer", "http://localhost:8002/project/5fa8e2184c975f0093d351a5");
      begin = System.nanoTime();
      timeStampTC1 = System.currentTimeMillis();
      response = httpClient.execute(request, httpContext);
      timeStampTC2 = System.currentTimeMillis();
      end = System.nanoTime();
    } catch (Exception e){
      System.err.println("IOException for httpDELETE");
      e.printStackTrace(System.err);
      System.exit(-1);
    }
    nanoseconds = end - begin;
    return response;
  }

  private String findCsrfToken(String doc){
    String pattern = "window.csrfToken = \"([^\"]+)\"";
    Pattern r = Pattern.compile(pattern);
    Matcher m = r.matcher(doc);
    if(m.find()){
      return m.group(1);
    } else{
      return null;
    }
  }
  private String findProjectID(String response){
    String pattern = "project_id\":\"([^\"]+)\"";
    Pattern r = Pattern.compile(pattern);
    Matcher m = r.matcher(response);
    if(m.find()){
      return m.group(1);
    } else{
      return null;
    }
  }

  private void setStates1(String message){
    String patternRootFolderID = "rootFolder\":\\[\\{\"_id\":\"([^\"]+)\"";
    String patternRootDocID = "rootDoc_id\":\"([^\"]+)\"";
    Pattern r = Pattern.compile(patternRootFolderID);
    Matcher m = r.matcher(message);
    if(m.find()){
      rootFolderID = m.group(1);
    } else{
      rootFolderID = "";
    }
    r = Pattern.compile(patternRootDocID);
    m = r.matcher(message);
    if(m.find()){
      rootDocID = m.group(1);
    } else{
      rootDocID = "";
    }
    logger.printLog("L3 rootFolderID", rootFolderID);
    logger.printLog("L3 rootDocID", rootDocID);

  }

  private void setStates2(String message){
    JSONArray msgArray;
    message = message.replace("6:::2+", "");
    try {
      JSONParser helper = new JSONParser();
      msgArray = (JSONArray)helper.parse(message);
      logger.printLog("L3 SONPARSE", msgArray.toJSONString());
      logger.printLog("L3 JSONPARSE[one]", msgArray.get(1).toString());
      JSONArray doc = (JSONArray)helper.parse(msgArray.get(1).toString());
      docVersion = msgArray.get(2).toString();
      docText = "";
      for(int i = 0; i < doc.size() - 1; i++){
        docText += doc.get(i).toString() + "\n";
      }
      docText += doc.get(doc.size() - 1);
      logger.printLog("L3 docText", docText);
      logger.printLog("L3 docVersion", docVersion);
    } catch (ParseException e) {
      System.err.println("ParseException in setStates");
      e.printStackTrace(System.err);
    }
  }

  private void updateVersion(String message){
    String patternDocVersion = "\"v\":(\\d+)";
    Pattern r = Pattern.compile(patternDocVersion);
    Matcher m = r.matcher(message);
    if(m.find()){
      docVersion = m.group(1);
      docVersion = "" + (Integer.parseInt(docVersion) + 1);
    } else{
      System.err.println("Error: No Version is found");
    }
    logger.printLog("L3 docVersion", docVersion);
    if(pendingText != null){
      docText = pendingText;
      logger.printLog("L3 new docText", docText);
      pendingText = null;
    }
  }

  /**
   * Error for failed commands.
   * */
  public class FailError extends Error{}
}

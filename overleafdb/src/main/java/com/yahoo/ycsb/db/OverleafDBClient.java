/**
 * Copyright (c) 2013 - 2016 YCSB contributors. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you
 * may not use this file except in compliance with the License. You
 * may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * permissions and limitations under the License. See accompanying
 * LICENSE file.
 */

package com.yahoo.ycsb.db;

import com.yahoo.ycsb.ByteIterator;
import com.yahoo.ycsb.DB;
import com.yahoo.ycsb.DBException;
import com.yahoo.ycsb.Status;
import com.yahoo.ycsb.db.workloads.OverleafCoreWorkload;
import com.yahoo.ycsb.measurements.Measurements;
import java.util.*;
import org.apache.http.client.methods.CloseableHttpResponse;

//import java.io.*;

// zevin{
/**
 * A class that wraps the OverleafDBClient to allow it to be interfaced with YCSB.
 * This class extends {@link DB} and implements the database interface used by YCSB client.
 */
public class OverleafDBClient extends DB {
  private final StringBuffer dumper = OverleafCoreWorkload.dumper;
  private OverleafCoreWorkload.Logger logger;
  private Measurements measurements = Measurements.getMeasurements();
  @Override
  public void init() throws DBException {
  }

  /**
   * Shutdown the client.
   */
  @Override
  public void cleanup() {
  }

  @Override
  public Status run(Object user, Map<String, Object> args) {
    OverleafClient overleafClient = (OverleafClient)user;
    CloseableHttpResponse response;
    long timeStampTC1 = 0;
    long timeStampTC2 = 0;
    long nanoseconds = 0;
    String printString = "";
    Status resultStatus;
    String command = args.get("command").toString();
    try{
      Status tmpStatus = null;
      printString = "command=" + command;
      switch (command){
      case "login_get":
        overleafClient.loginGetTask();
        break;
      case "login_post":
        overleafClient.loginPostTask();
        break;
      case "login":
        overleafClient.loginGetTask();
        timeStampTC1 = overleafClient.timeStampTC1;
        timeStampTC2 = overleafClient.timeStampTC2;
        nanoseconds = overleafClient.nanoseconds;
        measure(command + "_get_test-t" + overleafClient.threadID, Status.OK,
            timeStampTC1, timeStampTC2, nanoseconds);
        overleafClient.loginPostTask();
        timeStampTC1 = overleafClient.timeStampTC1;
        timeStampTC2 = overleafClient.timeStampTC2;
        nanoseconds = overleafClient.nanoseconds;
        measure(command + "_post_test-t" + overleafClient.threadID, Status.OK,
            timeStampTC1, timeStampTC2, nanoseconds);
        logger.printLog("L0 RUN" + "-t" + overleafClient.threadID, printString + " (" + nanoseconds + ")");
        return Status.OK;
      case "socket_get":
        overleafClient.socketGetTask();
        break;
      case "socket_connect":
        overleafClient.socketConnectTask();
        break;
      case "edit_doc":
        overleafClient.editDocTask();
        break;
      case "spell_check":
        overleafClient.spellCheckTask();
        break;
      case "chat_get":
        overleafClient.chatGetTask();
        break;
      case "chat_post":
        overleafClient.chatPostTask();
        break;
      case "email_get":
        overleafClient.emailGetTask();
        break;
      case "history_updates_get":
        overleafClient.historyUpdatesGetTask();
        break;
      case "history_diff_get":
        overleafClient.historyDiffGetTask();
        break;
      case "register_get":
        overleafClient.registerGetTask();
        break;
      case "settings_get":
        overleafClient.settingsGetTask();
        break;
      case "contacts_get":
        overleafClient.contactsGetTask();
        break;
      case "invites_get":
        overleafClient.invitesGetTask();
        break;
      case "invite_post":
        overleafClient.invitePostTask();
        break;
      case "project_with_id_get":
        overleafClient.projectIDGetTask();
        break;
      case "project_get":
        overleafClient.projectGetTask();
        break;
      case "compile_post":
        overleafClient.compilePostTask();
        break;
      case "create_delete_post":
        String projectID = overleafClient.createPostTask();
        timeStampTC1 = overleafClient.timeStampTC1;
        timeStampTC2 = overleafClient.timeStampTC2;
        nanoseconds = overleafClient.nanoseconds;
        measure(command + "_create-t" + overleafClient.threadID, Status.OK,
            timeStampTC1, timeStampTC2, nanoseconds);
        overleafClient.trashPostTask(projectID);
        timeStampTC1 = overleafClient.timeStampTC1;
        timeStampTC2 = overleafClient.timeStampTC2;
        nanoseconds = overleafClient.nanoseconds;
        measure(command + "_delete-t" + overleafClient.threadID, Status.OK,
            timeStampTC1, timeStampTC2, nanoseconds);
        logger.printLog("L0 RUN" + "-t" + overleafClient.threadID, printString + " (" + nanoseconds + ")");
        return Status.OK;
      default:
        printString = "NOT FOUND!!!!!! " + printString;
        tmpStatus = Status.ERROR;
      }
      resultStatus = tmpStatus == null ? Status.OK : tmpStatus;
    } catch (OverleafClient.FailError e){
      resultStatus = Status.ERROR;
    }
    timeStampTC1 = overleafClient.timeStampTC1;
    timeStampTC2 = overleafClient.timeStampTC2;
    nanoseconds = overleafClient.nanoseconds;
//    if(command.compareTo("edit_doc") == 0){
//      measure(command + "-t" + overleafClient.threadID, resultStatus,
//          timeStampTC1, timeStampTC2, nanoseconds, overleafClient.docVersion);
//    } else {
    measure(command + "-t" + overleafClient.threadID, resultStatus,
          timeStampTC1, timeStampTC2, nanoseconds);
//    }
    logger.printLog("L0 RUN" + "-t" + overleafClient.threadID, printString + " (" + nanoseconds + ")");
    return resultStatus;
  }

  @Override
  public final Status echo(final String words) {
    // task - begin
    long begin = System.nanoTime();
    System.err.println("run [ECHO] - " + words);

    // task - end
    long end = System.nanoTime();
    long nanoseconds = end - begin;
    dumper.append("echo-" + nanoseconds + "\n");
    logger.printLog("ECHO", " - " + nanoseconds);
    return Status.OK;
  }

  private void measure(String op, Status result, long timeStampTC1, long timeStampTC2, long nanoseconds) {
    String measurementName = op;
    if (result == null || !result.isOk()) {
      measurementName = op + "-FAILED";
    }
    dumper.append("run-" + measurementName 
        + "\tTC1=" + timeStampTC1
        + "\tTC2=" + timeStampTC2
        + "\tnanoseconds=" + nanoseconds + "\n");
    measurements.measure(measurementName,
        (int) (nanoseconds / 1000));
  }

  private void measure(String op,
                       Status result,
                       long timeStampTC1,
                       long timeStampTC2,
                       long nanoseconds,
                       String version) {
    String measurementName = op;
    if (result == null || !result.isOk()) {
      measurementName = op + "-FAILED";
    }
    dumper.append("run-" + measurementName
        + "\tmsTC1=" + timeStampTC1
        + "\tmsTC2=" + timeStampTC2
        + "\tv=" + version
        + "\tnanoseconds=" + nanoseconds + "\n");
    measurements.measure(measurementName,
        (int) (nanoseconds / 1000));
  }

  @Override
  public Status scan(final String dir, final String startkey, final int recordcount, final Set<String> fields,
                     final Vector<HashMap<String, ByteIterator>> result) {
    return Status.NOT_IMPLEMENTED;
  }
  @Override
  public Status update(final String dir, final String fileName, final Map<String, ByteIterator> values) {
    // append-only
    return Status.NOT_IMPLEMENTED;
  }
  @Override
  public Status insert(final String dir, final String fileName, final Map<String, ByteIterator> values) {
    return Status.NOT_IMPLEMENTED;
  }
  @Override
  public Status delete(final String table, final String key) {
    return Status.NOT_IMPLEMENTED;
  }
  @Override
  public Status read(final String dir, final String fileName, final Set<String> fields,
                     final Map<String, ByteIterator> result) {
    return Status.NOT_IMPLEMENTED;
  }

}
// zevin}

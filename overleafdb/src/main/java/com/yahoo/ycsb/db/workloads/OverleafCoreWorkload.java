package com.yahoo.ycsb.db.workloads;

import com.yahoo.ycsb.*;
import com.yahoo.ycsb.db.OverleafClient;
import com.yahoo.ycsb.generator.*;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Something.
 */
public class OverleafCoreWorkload extends Workload {

  // if flush the output to std
  public static final String LOG_LEVEL_PROPERTY = "loglevel";
  public static final String LOG_LEVEL_PROPERTY_DEFAULT = "0";
  public static int logLevel;

  // if proportion of echo operations
  public static final String IF_SINGLE_TASK_PROPERTY = "ifSingleTask";
  public static final String IF_SINGLE_TASK_PROPERTY_DEFAULT = "true";

  // if proportion of echo operations
  public static final String SINGLE_TASK_PROPERTY = "singleTask";
  public static final String SINGLE_TASK_PROPERTY_DEFAULT = "edit_doc";
  public static String singleTask;
  // if proportion of run operations
  public static final String RUN_PROPORTION_PROPERTY = "runproportion";
  public static final String RUN_PROPORTION_PROPERTY_DEFAULT = "0";

  // the path to dump the outputs such as latencies
  public static final String DUMPDIR_PROPERTY = "dumpDir";
  public static final String DUMPDIR_PROPERTY_DEFAULT = "dump";
  public static String dumpDir;

  public static StringBuffer dumper;
  public static boolean failedFlag = false;
  /**
   * The distribution of wait_time. options: constant, uniform, ...
   */
  public static final String WAIT_TIME_DISTRIBUTION_PROPERTY = "waitTimeDistribution";
  public static final String WAIT_TIME_DISTRIBUTION_PROPERTY_DEFAULT = "constant";

  /**
   * The lower and upper bound of wait_time, if uniform. options: 30 and 70, ...
   */
  public static final String WAIT_TIME_LOWER_BOUND = "waitTimeLowerBound";
  public static final String WAIT_TIME_LOWER_BOUND_DEFAULT = "30";
  public static final String WAIT_TIME_UPPER_BOUND = "waitTimeUpperBound";
  public static final String WAIT_TIME_UPPER_BOUND_DEFAULT = "70";

  /**
   * The constant wait_time. options: 50, 30, 70, ...
   */
  public static final String WAIT_TIME_CONSTANT = "waitTimeConstant";
  public static final String WAIT_TIME_CONSTANT_DEFAULT = "50";

  /**
   * The port of the target deployment.
   * options: 9443 for whisk19_mixed; 8002 for whisk18_microservice
   */
  public static final String TARGET_PORT = "targetPort";
  public static final String TARGET_PORT_DEFAULT = "8002";
  public static String targetPort;

  /**
   * The user id.
   */
  public static final String USER_ID_FOR_THREAD_PROPERTY = "userIDThread";
  public static final String USER_ID_FOR_THREAD_PROPERTY_DEFAULT = "5fa8c8996d9de206927ba5b1";

  /**
   * The user id.
   */
  public static final String USER_EMAIL_FOR_THREAD_PROPERTY = "userEmailThread";
  public static final String USER_EMAIL_FOR_THREAD_PROPERTY_DEFAULT = "king.zevin@qq.com";

  /**
   * The project id.
   */
  public static final String PROJECT_ID_FOR_THREAD_PROPERTY = "projectIDThread";
  public static final String PROJECT_ID_FOR_THREAD_PROPERTY_DEFAULT = "5fa8e2194c975f0093d351a8";

  /**
   * The doc_id.
   */
  public static final String DOC_ID_FOR_THREAD_PROPERTY = "docIDThread";
  public static final String DOC_ID_FOR_THREAD_PROPERTY_DEFAULT = "5fa8e2194c975f0093d351a9";

  @Override
  public void init(Properties p) throws WorkloadException {
    logLevel = Integer.parseInt(p.getProperty(LOG_LEVEL_PROPERTY, LOG_LEVEL_PROPERTY_DEFAULT));

    String os = System.getProperty("os.name");
    System.out.println(os + "!");

    dumpDir = p.getProperty(DUMPDIR_PROPERTY, DUMPDIR_PROPERTY_DEFAULT);
    if (os.compareTo("Mac OS X") == 0) {
      dumpDir = "/Users/king.zevin/code/" + dumpDir;
    } else{
      dumpDir = "/home/whisk/zevin/" + dumpDir;
    }
    dumper = new StringBuffer();

    targetPort = p.getProperty(TARGET_PORT, TARGET_PORT_DEFAULT);
  }

  @Override
  public Object initThread(Properties p, int mythreadid, int threadcount) throws WorkloadException {
    singleTask = p.getProperty(SINGLE_TASK_PROPERTY, SINGLE_TASK_PROPERTY_DEFAULT);
    NumberGenerator waitTimeChooser = createWaitTimeChooser(p);
    TaskGenerator taskChooser = createTaskChooser(p);

    String userID = p.getProperty(USER_ID_FOR_THREAD_PROPERTY + mythreadid,
        USER_ID_FOR_THREAD_PROPERTY_DEFAULT);
    String userEmail = p.getProperty(USER_EMAIL_FOR_THREAD_PROPERTY + mythreadid,
        USER_EMAIL_FOR_THREAD_PROPERTY_DEFAULT);
    String projectID = p.getProperty(PROJECT_ID_FOR_THREAD_PROPERTY + mythreadid,
        PROJECT_ID_FOR_THREAD_PROPERTY_DEFAULT);
    String docID = p.getProperty(DOC_ID_FOR_THREAD_PROPERTY + mythreadid,
        DOC_ID_FOR_THREAD_PROPERTY_DEFAULT);
    OverleafClient user = new OverleafClient(targetPort, userID, userEmail, projectID, docID, mythreadid, singleTask);
    return new ThreadState(waitTimeChooser, taskChooser, user);
  }

  @Override
  public void cleanup() {
    try {
      BufferedWriter out = new BufferedWriter(new FileWriter(dumpDir, false));
      out.write(dumper.toString());
      out.close();
      System.out.println(dumper.toString());
    } catch (IOException e){
      e.printStackTrace();
      System.err.println("[DUMP Failed] - " + dumpDir);
    }
  }

  @Override
  public boolean doInsert(DB db, Object threadstate){
    return true;
  }

  @Override
  public boolean doTransaction(DB db, Object threadstate){
    String command = nextCommand(threadstate);
    long waitTime = nextWaitTime(threadstate);
    if(command == null){
      return false;
    }
    try {
      Logger.printLog("L1 WAITING", waitTime + "ms.");
      Thread.sleep(waitTime);
    } catch (Exception e) {
      //TODO: handle exception
    }
    runCommand(db, threadstate, command);
    return true;
  }

  public void runCommand(DB db, Object threadstate, String command){
    Map<String, Object> args = new HashMap<>();
    args.put("command", command);
    db.run(((ThreadState)threadstate).user, args);
  }


  /**
   * Creates a weighted discrete taskset with operations for a workload to perform.
   * Weights/proportions are read from the properties list and defaults are used
   * when values are not configured.
   *
   * @param p The properties list to pull weights from.
   * @return A generator that can be used to determine the next operation to perform.
   * @throws IllegalArgumentException if the properties object was null.
   */
  protected TaskGenerator createTaskChooser(final Properties p) {
    if (p == null) {
      throw new IllegalArgumentException("Properties object cannot be null");
    }
    final boolean singleTaskFlag = Boolean.parseBoolean(
        p.getProperty(IF_SINGLE_TASK_PROPERTY, IF_SINGLE_TASK_PROPERTY_DEFAULT));

    final TaskGenerator taskChooser = new TaskGenerator();

    Task loginPostTask = new Task(1, "login_post");
    Task loginGetTask = new Task(1, "login_get");
    loginGetTask.addTask(loginPostTask);
    taskChooser.addTask(loginGetTask);

    Task socketConnectTask = null;
    Task socketGetTask = null;
    Task chatGetTask = null;
    Task chatPostTask = null;
    if(singleTaskFlag){
      switch(singleTask){
      case "spell_check":
        Task spellCheckTask = new Task(1, "spell_check");
        loginPostTask.addTask(spellCheckTask);
        break;
      case "login":
        Task loginTask = new Task(1, "login");
        loginPostTask.addTask(loginTask);
        break;
      case "email_get":
        Task emailGetTask = new Task(1, "email_get");
        loginPostTask.addTask(emailGetTask);
        break;
      case "edit_doc":
        Task editDocTask = new Task(1, "edit_doc");
        socketConnectTask = new Task(1, "socket_connect");
        socketConnectTask.addTask(editDocTask);
        socketGetTask = new Task(1, "socket_get");
        socketGetTask.addTask(socketConnectTask);
        loginPostTask.addTask(socketGetTask);
        break;
      case "create_delete_post":
        Task createDeletePostTask = new Task(1, "create_delete_post");
        loginPostTask.addTask(createDeletePostTask);
        break;
      case "history_updates_get":
        Task historyUpdatesGetTask = new Task(1, "history_updates_get");
        loginPostTask.addTask(historyUpdatesGetTask);
        break;
      case "history_diff_get":
        Task historyDiffGetTask = new Task(1, "history_diff_get");
        loginPostTask.addTask(historyDiffGetTask);
        break;
      case "register_get":
        Task registerGetTask = new Task(1, "register_get");
        loginPostTask.addTask(registerGetTask);
        break;
      case "settings_get":
        Task settingsGetTask = new Task(1, "settings_get");
        loginPostTask.addTask(settingsGetTask);
        break;
      case "contacts_get":
        Task contactsGetTask = new Task(1, "contacts_get");
        loginPostTask.addTask(contactsGetTask);
        break;
      case "invites_get":
        Task invitesGetTask = new Task(1, "invites_get");
        loginPostTask.addTask(invitesGetTask);
        break;
      case "invite_post":
        Task invitePostTask = new Task(1, "invite_post");
        loginPostTask.addTask(invitePostTask);
        break;
      case "project_get":
        Task projectGetTask = new Task(1, "project_get");
        loginPostTask.addTask(projectGetTask);
        break;
      case "project_with_id_get":
        Task projectIDGetTask = new Task(1, "project_with_id_get");
        loginPostTask.addTask(projectIDGetTask);
        break;
      case "chat_get":
        chatGetTask = new Task(1, "chat_get");
        socketConnectTask = new Task(1, "socket_connect");
        socketConnectTask.addTask(chatGetTask);
        socketGetTask = new Task(1, "socket_get");
        socketGetTask.addTask(socketConnectTask);
        loginPostTask.addTask(socketGetTask);
        break;
      case "chat_post":
        chatPostTask = new Task(1, "chat_post");
        socketConnectTask = new Task(1, "socket_connect");
        socketConnectTask.addTask(chatPostTask);
        socketGetTask = new Task(1, "socket_get");
        socketGetTask.addTask(socketConnectTask);
        loginPostTask.addTask(socketGetTask);
        break;
      case "chat":
        chatGetTask = new Task(1, "chat_get");
        chatPostTask = new Task(1, "chat_post");
        socketConnectTask = new Task(1, "socket_connect");
        socketConnectTask.addTask(chatGetTask);
        socketConnectTask.addTask(chatPostTask);
        socketGetTask = new Task(1, "socket_get");
        socketGetTask.addTask(socketConnectTask);
        loginPostTask.addTask(socketGetTask);
        break;
      case "compile_post":
        Task compilePostTask = new Task(1, "compile_post");
        socketConnectTask = new Task(1, "socket_connect");
        socketConnectTask.addTask(compilePostTask);
        socketGetTask = new Task(1, "socket_get");
        socketGetTask.addTask(socketConnectTask);
        loginPostTask.addTask(socketGetTask);
        break;
      default:
        Logger.printLog("L0 TASK", "NOT FOUND!!!!!! " + singleTask);
        System.exit(-1);
      }
    } else{
      Logger.printLog("L0 TASK", "MULTI-TASK NOT SUPPORTED!!!!!! ");
      System.exit(-1);
    }

    return taskChooser;
  }

  protected static NumberGenerator createWaitTimeChooser(final Properties p) {
    if (p == null) {
      throw new IllegalArgumentException("Properties object cannot be null");
    }
    NumberGenerator waitTimeChooser = null;

    // wait time
    String waitTimeDistribution =
        p.getProperty(WAIT_TIME_DISTRIBUTION_PROPERTY, WAIT_TIME_DISTRIBUTION_PROPERTY_DEFAULT);
    if (waitTimeDistribution.compareTo("uniform") == 0) {
      // wait time: [low, upper]
      long waitTimeLowerBound = Long.parseLong(p.getProperty(WAIT_TIME_LOWER_BOUND, WAIT_TIME_LOWER_BOUND_DEFAULT));
      long waitTimeUpperBound = Long.parseLong(p.getProperty(WAIT_TIME_UPPER_BOUND, WAIT_TIME_UPPER_BOUND_DEFAULT));
      waitTimeChooser = new UniformLongGenerator(waitTimeLowerBound, waitTimeUpperBound);
    } else if (waitTimeDistribution.compareTo("constant") == 0) {
      // wait time: constant
      int waitTimeConstant = Integer.parseInt(p.getProperty(WAIT_TIME_CONSTANT, WAIT_TIME_CONSTANT_DEFAULT));
      waitTimeChooser = new ConstantIntegerGenerator(waitTimeConstant);
    }
    return waitTimeChooser;
  }

  long nextWaitTime(Object threadstate){
    NumberGenerator waitTimeChooser = ((ThreadState)threadstate).waitTimeChooser;
    long waitTime;
    waitTime = waitTimeChooser.nextValue().intValue();
    return waitTime;
    // if (waitTimeChooser instanceof ExponentialGenerator) {
    //   do {
    //     // waitTime = transactioninsertkeysequence.lastValue() - waitTimeChooser.nextValue().intValue();
    //   } while (waitTime < 0);
    // } else {
    //   do {
    //     waitTime = waitTimeChooser.nextValue().intValue();
    //   } while (waitTime > transactioninsertkeysequence.lastValue());
    // }
  }

  String nextCommand(Object threadstate){
    return ((ThreadState)threadstate).taskChooser.nexCommand();
  }

  /**
   * A debug logger.
   */
  public static class Logger{
    public static void printLog(String tag, String log){
      int sentenceLevel = 3;
      if(tag.contains("L9")){
        System.out.println("\u001B[34m[LOG-" + tag + "]\u001B[0m " + log);
        return;
      }
      if(tag.contains("L1")){
        sentenceLevel = 1;
      } else if(tag.contains("L2")){
        sentenceLevel = 2;
      } else if(tag.contains("L0")){
        sentenceLevel = 0;
      }

      if(sentenceLevel > logLevel){
        return;
      }
      System.out.println("\u001B[34m[LOG-" + tag + "]\u001B[0m " + log);
    }
  }

  protected class ThreadState{
    NumberGenerator waitTimeChooser;
    TaskGenerator taskChooser;
    OverleafClient user;

    protected ThreadState(NumberGenerator waitTimeChooser,
                          TaskGenerator taskChooser,
                          OverleafClient user){
      this.waitTimeChooser = waitTimeChooser;
      this.taskChooser = taskChooser;
      this.user = user;
    }
  }

  /**
   * A task set generator based on DiscreteGenerator.
   *
   * */
  public class TaskGenerator {
    private Collection<Task> subTaskSet = new ArrayList<>();
    private String lastTask;
    public TaskGenerator(){ }
    public void addTask(Task t){
      subTaskSet.add(t);
    }

    public String nexCommand() {
      printTasks();
      double sum = 0;
      for(Task t: subTaskSet){
        sum += t.weight;
      }

      double val = ThreadLocalRandom.current().nextDouble();

      for (Task t: subTaskSet){
        double tw = t.weight / sum;
        Logger.printLog("L3 TW", "tw=" + tw + ";val=" + val);
        if(val < tw){
          // chosen
          if(t.subTaskSet.size() == 0){
            // no change if no child task
            return t.command;
          }
          double subSum = 0;
          for (Task tt: t.subTaskSet){
            subSum += tt.weight;
          }
          for (Task tt: t.subTaskSet){
            tt.weight = tt.weight / subSum * t.weight; // new weight
            subTaskSet.add(tt);
          }
          subTaskSet.remove(t);
          return t.command;
        }

        val -= tw;
      }

      throw new AssertionError("oops. should not get here.");
    }
    private void printTasks(){
      for(Task t: subTaskSet){
        printTask(t, 0);
      }
    }
    private void printTask(Task t, int indent){
      String tabs = "";
      for(int i = 0; i < indent; i++){
        tabs +="\t";
      }
      Logger.printLog("L0 Task", tabs + "command=" + t.command + "; weight=" + t.weight);
      for(Task tt: t.subTaskSet){
        printTask(tt, indent + 1);
      }
    }
  }



  /**
   * Something.
   * */
  public class Task{
    public double weight;
    public String command;
    public Collection<Task> subTaskSet;
    Task(double weight, String command){
      this.weight = weight;
      this.command = command;
      this.subTaskSet = new ArrayList<>();
    }
    public void addTask(Task t){
      subTaskSet.add(t);
    }
  }
}
